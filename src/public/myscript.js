var myVideoArea = document.querySelector("#myVideoTag");
var videoSelect = document.querySelector('#camera');
var myName = document.querySelector('#myName');
var myMessage = document.querySelector('#myMessage');
var sendMessage = document.querySelector('#sendMessage');
var chatArea = document.querySelector('#chatArea');
var theirVideoArea = document.querySelector('#theirVideoTag');
var signalingArea = document.querySelector('#signalingArea');


var configuration = {
    iceServers: [{
            url: 'stun:stun01.sipphone.com'
        },
        {
            url: 'stun:stun.ekiga.net'
        },
        {
            url: 'stun:stun.fwdnet.net'
        },
        {
            url: 'stun:stun.ideasip.com'
        },
        {
            url: 'stun:stun.iptel.org'
        },
        {
            url: 'stun:stun.rixtelecom.se'
        },
        {
            url: 'stun:stun.schlund.de'
        },
        {
            url: 'stun:stun.l.google.com:19302'
        },
        {
            url: 'stun:stun1.l.google.com:19302'
        },
        {
            url: 'stun:stun2.l.google.com:19302'
        },
        {
            url: 'stun:stun3.l.google.com:19302'
        },
        {
            url: 'stun:stun4.l.google.com:19302'
        },
        {
            url: 'stun:stunserver.org'
        },
        {
            url: 'stun:stun.softjoys.com'
        },
        {
            url: 'stun:stun.voiparound.com'
        },
        {
            url: 'stun:stun.voipbuster.com'
        },
        {
            url: 'stun:stun.voipstunt.com'
        },
        {
            url: 'stun:stun.voxgratia.org'
        },
        {
            url: 'stun:stun.xten.com'
        },
        {
            url: 'turn:numb.viagenie.ca',
            credential: 'muazkh',
            username: 'webrtc@live.com'
        },
        {
            url: 'turn:192.158.29.39:3478?transport=udp',
            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            username: '28224511:1379330808'
        },
        {
            url: 'turn:192.158.29.39:3478?transport=tcp',
            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            username: '28224511:1379330808'
        }
    ]
};

var rtcPeerConn;



var ROOM = 'chat';

var SIGNAL_ROOM = "signal_room"

function displayMessage(msg) {
    chatArea.innerHTML += msg + "<br/>";
}

function displaySignalMessage(msg) {
    signalingArea.innerHTML += msg + "<br/>";
}






soc = io.connect();
soc.emit('ready', {
    chat_room: ROOM,
    sig_room: SIGNAL_ROOM
});
soc.on('announce', function (data) {
    displayMessage(data.message);
});
soc.on('message', function (data) {
    displayMessage(data.author + " : " + data.message);
});

soc.emit('signal', {
    type: "user_here",
    message: "Are you Ready?",
    room: SIGNAL_ROOM
});

soc.on('disconnecteduser',function(data){
    var sourceObject = HTMLMediaElement.srcObject;
    displaySignalMessage("Removing to add their stream. . .");
        theirVideoArea.srcObject = sourceObject;
        myVideoArea.srcObject =sourceObject;
        window.location.reload();
})


sendMessage.addEventListener('click', function (ev) {
    soc.emit('send', {
        "author": myName.value,
        "message": myMessage.value,
        "room": ROOM
    })
    ev.preventDefault();
}, false);

soc.on('signaling_message', function (data) {
    displaySignalMessage("SignalRecived " + data.type);

    //update rtcpeerconn obj
    if (!rtcPeerConn) {
        startSignalling();
    }
    if (data.type != "user_here") {
        var message = JSON.parse(data.message);
        if (message.sdp) {
            rtcPeerConn.setRemoteDescription(new RTCSessionDescription(message.sdp)).then(function () {
                //if we recived offer we need to answer
                if (rtcPeerConn.remoteDescription.type == 'offer') {
                    rtcPeerConn.createAnswer(setLocalDesc, logError);
                }
            }).catch(logError);
        } else {
            rtcPeerConn.addIceCandidate(new RTCIceCandidate(message.candidate));
        }

    }
});


function setLocalDesc(desc) {
    rtcPeerConn.setLocalDescription(desc, function () {
        displaySignalMessage("sending local description");
        soc.emit('signal', {
            type: "SDP",
            message: JSON.stringify({
                sdp: rtcPeerConn.localDescription
            }),
            room: SIGNAL_ROOM
        })
    });
}

function logError(error) {
    displaySignalMessage(error.name + " : " + error.message);
}



function startSignalling() {
    displaySignalMessage("Start signalling . . .");
    var RTCPeerConn = window.RTCPeerConnection || window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
    rtcPeerConn = new RTCPeerConn(configuration)

    //send any ice candidates to the other peer
    rtcPeerConn.onicecandidate = function (evt) {
        if (evt.candidate)
            soc.emit('signal', {
                "type": "ice candidate",
                message: JSON.stringify({
                    "candidate": evt.candidate
                }),
                "room": SIGNAL_ROOM
            });
        displaySignalMessage("Completed the ice candidate . . .");
    };

    //let the 'negoiationneeded' event trigger offer generation
    rtcPeerConn.onnegotiationneeded = function () {
        displaySignalMessage("on negotiation called");
        rtcPeerConn.createOffer(setLocalDesc, logError)
    }

    //once remote stream arrives, show it in the remote video element
    rtcPeerConn.onaddstream = function (evt) {
        displaySignalMessage("going to add their stream. . .");
        theirVideoArea.srcObject = evt.stream;
    }

    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

    navigator.getUserMedia({
        audio: true,
        video: {
            mandatory: {
                minWidth: 480,
                maxWidth: 480,
                minHeight: 480,
                minHeight: 480
            }
        }
    }, function (stream) {
        displaySignalMessage("going to display my stream . . .");
        myVideoArea.srcObject = stream;
        rtcPeerConn.addStream(stream);
    }, logError);

};
