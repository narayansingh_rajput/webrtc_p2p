var express = require('express');
var app = express();
var path = require('path');
var port =process.env.PORT || 3000;
app.use(express.static(__dirname + '/public'));
// app.set('views', path.join(__dirname, 'views'));
app.get('/', function (req, res) {
    res.render('index.ejs');
});


var http = require('http').Server(app);
var io = require('socket.io')(http);

io.on('connection', function (socket) {
    socket.on('ready', function (data) {
        socket.join(data.chat_room);
        socket.join(data.sig_room);
        io.to(data.chat_room).emit('announce', {
            message: 'New User in the ' + data.chat_room + 'room.'
        });
    });

    socket.on('send', function (data) {
        io.to(data.room).emit('message', {
            "author": data.author,
            "message": data.message
        });
    });

    socket.on('signal', function (data) {
        // console.log(data);
        socket.broadcast.to(data.room).emit('signaling_message', {
            "type": data.type,
            "message": data.message
        });
    });

    socket.on('disconnect', function () {
        socket.broadcast.emit('disconnecteduser', {
            "type": 'disconnected'
        });
    });




});



http.listen(port, function (err) {
    if (err) {
        console.log(err);
    }
    console.log("Server started @ "+port);
});
